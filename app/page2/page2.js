function page2Controller($stateParams){
	this.onStart=function($stateParams){
		console.info("app/page2/page2.js - page2Controller - onStart");	
		this.prepare();
	}
	
	this.onRestart=function($stateParams){
		console.info("app/page2/page2.js - indexController - onRestart");
		this.prepare();
	}
	
	this.prepare=function(){
		mc.hideWait();
		mc.leftMenuSelected="page2";
		mc.title="Estamos en page 2";
	}
}