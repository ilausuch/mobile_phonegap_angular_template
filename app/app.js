/**
Angular application template
author: Ivan Lausuch Sales
Started on 2014
*/

console.info("----------------- New execution -----------------");

//----------------------------------------------------------------------------------
// ANGULAR APPLICATION
//----------------------------------------------------------------------------------

//TODO : All all needed modules
//NOTE: In mobile apps is required ["ui.router","ngTouch","mobile-angular-ui"]
var app = angular.module('app', ["ui.router","ngMaterial"]);


//----------------------------------------------------------------------------------
// MAIN CONTROLER
//----------------------------------------------------------------------------------
//Create default main controller
var mc;

app.controller("MainController", function($rootScope,$scope,$timeout,$http,$q,$mdSidenav,$mdDialog){
	
	//Set mc global variable
	mc=this;
	
	//Easy access to main controls
	this.$rootScope=$rootScope;
	this.$scope=$scope;
	this.$timeout=$timeout;
	this.$http=$http;
	this.$q=$q;
	this.$mdSidenav=$mdSidenav;
	this.$mdDialog=$mdDialog;
	this.c=undefined;
	this.route={};
	
	//Complete main controller with functions
	completeMainController();
	
	//TODO : Define here all variables or functions you need
	this.leftMenuSelected="default";
	this.title="Default";
	
	//TODO : onReady event. It is called when all are ready
	this.onReady=function(){
		console.info("app/app.js - MainController - onReady - Implement here user check or other needs");
	}
	
	this.toggleLeftMenu = function() {
		$mdSidenav('menuLeft').toggle();
	};
	
	//Force to start Main controller
	mc.start();
});


//----------------------------------------------------------------------------------
// ROUTER CONTROLER
//----------------------------------------------------------------------------------

/**
This is route configuration. Is where you define all routes in your application
*/
app.config(function($stateProvider, $urlRouterProvider) {
    //Default route
    $urlRouterProvider.otherwise('/');

    //TODO : Define all routes ($stateProvider, route(string), controller(string) [, persistent(bool) ,prerequisitesChecker(func)])
    /*
	NOTE:PrerequisitesChecker must be a funcion with these params (callback,$stateParams,$location) 
	and mush call to callback when finish
	*/
    stateRouteAdd($stateProvider,"/","index",true,
    	function(callback,$stateParams,$location){
	    	console.info("app/app.js - Route config - / - Prerequisites checker (remove if you don't need it)");
	    	callback();	
	    }
	);  
	
	stateRouteAdd($stateProvider,"/page2","page2");
	
	
});